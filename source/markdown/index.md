## Work gamified

Meet @qitten: a Slack bot that gamifies work and makes it more fun.

It understands a few commands that help recognize and memorialize achievements and outstanding feats.

### Points

Reward your colleagues for their achievements by giving them points.

{{#data:_chat/reward.csv#template:_chat/_template.html#parent:chat}}

### Titles

Record funny situations or unexpected gifts and personality traits with titles

{{#data:_chat/titles.csv#template:_chat/_template.html#parent:chat}}

{{#data:_chat/whois.csv#template:_chat/_template.html#parent:chat}}

## Features

### Adding to channels

Qitten only works in channels it's added to.

You can either [add qitten manually](/add_to_channel/) to each channel you'd like it to be active in, or auto-add it to all public channels:

```
@qitten join all
```

### In-app instructions

Read about the supported commands here, or from qitten itself:

```
@qitten commands
```

### Points

The details of working with points can be found here. Here are the most important commands:

```
@username++
@username--
@qitten points @username
@qitten leaderboard
```

### Titles

Titles help celebrate achievements, express gratitude or acknowledge silly quirks. They're added one by one, and can be listed out in the order they were added. Here's the gist of working with titles:

```
@qitten @username is a smart and silly cat
@qitten @username is not a smart and silly cat
@qitten who is @username
```

### Nicknames

Nicknames can be used as a shorthand for giving someone points or titles. Here are the basic commands:

```
@qitten nickname add @VeryLongUserName shortname
@qitten nickname remove @VeryLongUserName shortname
@qitten nickname list @username
```
