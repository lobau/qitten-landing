# Points

Any @username can have points, but also any single-word label of characters, digits, underscores.

### Commands

In a channel with qitten, the commands below will give points or take them away:
```
@username++
@username--
any_label++
any_label--
# Give points to multiple people at once:
@user1++ @user2 squirrels++ @user3-- winter--
# The @qitten syntax will work too:
@qitten any_label++ any_label--
@qitten @username++ @username--
# User group points go to everyone in the user group:
@usergroup++
@usergroup--
```

Points work with [nicknames](/nickname_instruction/). If multiple people share a nickname, they will all get points for that nickname.

The commands below get the current point counts:
```
@qitten points @username
@qitten points label
@qitten leaderboard
@qitten leaderboard 12
```

### Examples

Giving and taking points
<!-- ![TODO alt](/static/img/points/multiple_points.png) -->
{{#data:_chat/multiple_points.csv#template:_chat/_template.html#parent:chat}}

Leaderboard
<!-- ![TODO alt](/static/img/points/leaderboard.png) -->
{{#data:_chat/leaderboard.csv#template:_chat/_template.html#parent:chat}}

Shared nickname getting points. More info in the [nicknames](/nickname_instruction/) section.
<!-- ![TODO alt](/static/img/points/multi_nickname_points.png) -->
{{#data:_chat/group_points.csv#template:_chat/_template.html#parent:chat}}

Assigning points to everyone in a user group
<!-- ![TODO alt](/static/img/points/usergroup_points.png) -->
{{#data:_chat/usergroup_points.csv#template:_chat/_template.html#parent:chat}}
