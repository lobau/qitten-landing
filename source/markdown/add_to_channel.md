# Adding qitten to a channel

Qitten only works in channels it's explicitly added to.

It can either be added manually to one channel at a time, or bulk-added to all public channels.


### Adding qitten Automatically
```
@qitten join all
```

This will only work to join internal public channels - i.e., no group conversations, no shared channels, no private channels.


### Adding qitten Manually

When you're in a channel, you'll see a dropdown menu with the channel name under the search bar

<!--![TODO caption](/static/img/channel_add/channel_name.png)-->
![TODO caption](/static/img/slack_placeholder.svg)

When you click it, you'll see a popup. Go to the "integrations" tab, and click "Add an App":

<!-- ![TODO caption](/static/img/channel_add/integrations_tab.png) -->
![TODO caption](/static/img/slack_placeholder.svg)

After that, find the bot by scrolling, or typing its name in the search bar:

<!-- ![TODO caption](/static/img/channel_add/add_through_search.png) -->
![TODO caption](/static/img/slack_placeholder.svg)

