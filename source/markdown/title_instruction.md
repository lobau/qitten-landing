# Titles

### Commands

In a channel with qitten, titles can be given, taken away, and listed. Like [points](/points_instruction/), they can be applied to a user or any label:

```
@qitten @username is a biscuit afocionado
@qitten any_label is a nap room

# Listing titles:
@qitten who is @username
@qitten who is any_label

# Removing titles - title must match exactly:
@qitten @username is not a biscuit afocionado
@qitten any_label is not a nap room
```

Titles work with [nicknames](/nickname_instruction/) - but if a nickname is shared by multiple users, the title won't be assigned to anyone.

## Examples

### Adding a title

{{#data:_chat/titles.csv#template:_chat/_template.html#parent:chat}}

### Removing a title

{{#data:_chat/title_remove.csv#template:_chat/_template.html#parent:chat}}

### Listing titles

{{#data:_chat/titles_list.csv#template:_chat/_template.html#parent:chat}}

### Adding titles for an overloaded nickname won't work

{{#data:_chat/group_title.csv#template:_chat/_template.html#parent:chat}}
