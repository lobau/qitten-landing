# Nicknames

### Commands

Nicknames can be used as a shorthand for giving points and titles to users:
```
@qitten @username nickname add cupcake
@qitten @username nickname remove thor
@qitten @username nickname list
```

A user may have any number of nicknames, and any of a user's nicknames can be used to give them points or titles.

### Nuances
#### Userless nicknames

Since points and titles can be assigned to any label - not just to users - a nickname may already have points/titles before it's assigned to a user.

In that situation - *any pre-existing points/titles will be transfered to the user receiving the nickname.*

#### Shared nicknames

A user can have any number of nicknames, and the same nickname can be shared among many users:

- If a point is given to a nickname shared by many users, they will all get the point.
- If a title is given to a nickname shared by many users, none of them will get the title.

#### Auto-populated nicknames

If a user is @-mentioned for the first time without having a prior nickname, qitten attempts to auto-populate the nickname from the user's Slack profile.

It's not always possible: users may not have their Slack information filled out, or qitten may not have permission to fetch the nickname (e.g. for external users in shared channels).

### Examples

Adding a nickname
{{#data:_chat/add_nickname.csv#template:_chat/_template.html#parent:chat}}

Removing
{{#data:_chat/remove_nickname.csv#template:_chat/_template.html#parent:chat}}

Listing
{{#data:_chat/listing.csv#template:_chat/_template.html#parent:chat}}

Points to a group of people
{{#data:_chat/group_points.csv#template:_chat/_template.html#parent:chat}}


Titles can't be assigned to a group of people
{{#data:_chat/group_title.csv#template:_chat/_template.html#parent:chat}}


But if only one person has a nickname, adding a title will work:
{{#data:_chat/title_nickname.csv#template:_chat/_template.html#parent:chat}}

